var app = app || {};

app.init = function () {

	app.smoothScroll();

	app.backTop();

	app.switchNav();

	app.slideShow();
	app.addMoreTrans();
	app.deleteTrans();
	app.editTrans();

	if($(":file:not(.file-attach)").length > 0) {
		$(":file:not(.file-attach)").filestyle({
			buttonBefore: true,
			buttonText: "",
			iconName: "fa fa-camera"
		});
	}

	if($(".file-attach").length > 0) {
		$(".file-attach").filestyle({
			buttonBefore: true,
			buttonText: "file đính kèm",
			iconName: "fa fa-paperclip"
		});
	}

	app.slideStep();

}

// Switch Nav
app.switchNav = function() {
	var w = $(window).width();
	var slideoutMenu = $('#navigation');
	var btn_menu = $('.btn-menu');
	var overlay = $('.overlay');
	var h_nav = $("#menu-collapse")

	$('.btn-menu').on('click', function(event){
		
		// toggle open class
		$(this).toggleClass("open");
		
		// slide menu
		if (btn_menu.hasClass("open")) {
			slideoutMenu.slideDown(300);
			overlay.fadeIn('slow');
		} else {
			slideoutMenu.slideUp();
			overlay.fadeOut();
		}
		return false;
	});

	$('.group-menu-item h4').on('click', function(event){
		$(this).toggleClass("collapse");
		$(this).next().stop().slideToggle(300);
	});

}

// Smooth scroll
app.smoothScroll = function() {
	$('a.scroll').click(function() {
		var speed = 500;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
}


// Page slider
app.slideShow = function() {
	if($('.bx-slider').length > 0) {
		$(".bx-slider").bxSlider({
			mode 		: 'vertical',
			auto		: true,
			speed		: 1000,
			pause		: 5000,
			controls 	: false,
			pager		: true
		});
	}
}


// Step slider
app.slideStep = function() {
	if($('.slide-step').length > 0) {
		var slider = $(".slide-step").bxSlider({
			// mode 		: 'fade',
			auto		: true,
			speed		: 1000,
			pause		: 3000,
			controls 	: false,
			pagerCustom	: "#pager"
		});

		$("#pager a").on('click', function() {
			var index = $(this).attr('data-slide-index');
			var icon = $(this).find('.icon').has("img");

			slider.goToSlide(index);
		});
	}
}

/* BACK TO TOP
==============================*/
app.backTop = function() {
	var backTop = $('#backtop');
	backTop.hide();
	$(window).scroll(function() {
		if($(window).scrollTop() > 300) {
			backTop.fadeIn();
		} else {
			backTop.fadeOut();
		}
	});

	$('#backtop').click(function() {
		$('html, body').animate({scrollTop:0},500);
		return false;
	});
}

app.addMoreTrans = function(){
	$("#btn-add-trans").on('click', function() {
		var wrapper = $(this).parents('form');
		var typeTrans = wrapper.find('#type-trans').val();
		var numberTrans = wrapper.find('#number-trans').val();
		var fileImage = wrapper.find('#img-tmp').val();
		var certificateTrans = wrapper.find('#certificate-trans').val();
		var numberGuarantee = wrapper.find('#number-guarantee').val();
		var numberCheck = wrapper.find('#number-check').val();
		var fullname = wrapper.find('#fullname').val();
		var certificateDrive = wrapper.find('#certificate-drive').val();
		var idCard = wrapper.find('#id-card').val();
		var lastRow = $('.table-responsive tbody tr').last().attr('data-number');
		var HTML = '<tr data-number="'+(parseInt(lastRow) + 1)+'">';
		HTML += '<td class="type-trans">'+typeTrans+'</td>';
		HTML += '<td class="number-trans">'+numberTrans+'</td>';
		HTML += '<td class="file-image"><img style="width:70px" src="'+fileImage+'" /></td>';
		HTML += '<td class="certificate-trans">'+certificateTrans+'</td>';
		HTML += '<td class="number-guarantee">'+numberGuarantee+'</td>';
		HTML += '<td class="number-check">'+numberCheck+'</td>';
		HTML += '<td class="fullname">'+fullname+'</td>';
		HTML += '<td class="certificate-drive">'+certificateDrive+'</td>';
		HTML += '<td class="id-card">'+idCard+'</td>';
		HTML += '<td><a href="#edit-trans" title="Sửa" class="edit-row" data-toggle="modal"><i class="fa fa-pencil"></i></a>&nbsp;<a href="javascript:void(0);" title="Xóa" class="del-row"><i class="fa fa-trash"></i></a></td>';
		HTML += '</tr>';
		$('.table-responsive tbody').append(HTML);
	});

	var fileInput = $('#file-image');

    fileInput.change(function (event) {
    	var tmppath = URL.createObjectURL(event.target.files[0]);      
        $('#img-tmp').val(tmppath);       
    });
}

app.deleteTrans = function(){
	$("body").on('click','.del-row', function(e) {	
		e.preventDefault();
		if(confirm("Bạn có chắc chắn xóa!")) {
			$(this).parents('tr').remove();
		}
	});
}

app.editTrans = function(){
	$("body").on('click','.edit-row', function(e) {	
		e.preventDefault();
		var wrapper = $(this).parents('tr');
		var typeTrans = wrapper.find('.type-trans').text();
		var numberTrans = wrapper.find('.number-trans').text();		
		var certificateTrans = wrapper.find('.certificate-trans').text();
		var numberGuarantee = wrapper.find('.number-guarantee').text();
		var numberCheck = wrapper.find('.number-check').text();
		var fullname = wrapper.find('.fullname').text();
		var certificateDrive = wrapper.find('.certificate-drive').text();
		var idCard = wrapper.find('.id-card').text();
		
		$('#edit-trans').find('#type-trans').val(typeTrans);
		$('#edit-trans').find('#number-trans').val(numberTrans);		
		$('#edit-trans').find('#certificate-trans').val(certificateTrans);
		$('#edit-trans').find('#number-guarantee').val(numberGuarantee);
		$('#edit-trans').find('#number-check').val(numberCheck);
		$('#edit-trans').find('#fullname').val(fullname);
		$('#edit-trans').find('#certificate-drive').val(certificateDrive);
		$('#edit-trans').find('#id-card').val(idCard);
		$('#edit-trans').find('#id-row').val(wrapper.attr('data-number'));
	});

	$("body").on('click','#btn-update-trans', function(e) {	
		
		var row = $('.table-responsive tbody').find('tr[data-number="'+$(this).siblings('#id-row').val()+'"]');

		var wrapper = $(this).parents('form');
		var typeTrans = wrapper.find('#type-trans').val();
		var numberTrans = wrapper.find('#number-trans').val();
		var fileImage = wrapper.find('#img-tmp').val();
		var certificateTrans = wrapper.find('#certificate-trans').val();
		var numberGuarantee = wrapper.find('#number-guarantee').val();
		var numberCheck = wrapper.find('#number-check').val();
		var fullname = wrapper.find('#fullname').val();
		var certificateDrive = wrapper.find('#certificate-drive').val();
		var idCard = wrapper.find('#id-card').val();
		var fileImage = wrapper.find('#img-tmp-update').val();
		
		row.find('.type-trans').text(typeTrans);
		row.find('.number-trans').text(numberTrans);		
		row.find('.certificate-trans').text(certificateTrans);
		row.find('.number-guarantee').text(numberGuarantee);
		row.find('.number-check').text(numberCheck);
		row.find('.file-image').html('<img src="'+fileImage+'" style="width:70px"/>');
		row.find('.fullname').text(fullname);
		row.find('.certificate-drive').text(certificateDrive);
		row.find('.id-card').text(idCard);

	});

	var fileInput = $('#file-image-update');

    fileInput.change(function (event) {
    	var tmppath = URL.createObjectURL(event.target.files[0]);      
        $('#img-tmp-update').val(tmppath);       
    });


}

$(function() {

	app.init();

});
